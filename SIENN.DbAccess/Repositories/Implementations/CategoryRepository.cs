﻿using SIENN.Models;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.DTOs;
using AutoMapper;

namespace SIENN.DbAccess.Repositories.Implementations
{
    public class CategoryRepository : GenericRepository<CategoryDTO, Category>, ICategoryRepository
    {
        public CategoryRepository(SIENNContext context, IMapper mapper) : base(context, mapper) { }
    }
}
