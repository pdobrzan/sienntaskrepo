﻿using System.Collections.Generic;
using AutoMapper;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.DTOs;
using SIENN.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace SIENN.DbAccess.Repositories.Implementations
{
    public class ProductRepository : GenericRepository<ProductDTO, Product>, IProductRepository
    {
        public ProductRepository(SIENNContext context, IMapper mapper) : base(context, mapper) { }

        public IEnumerable<ProductDTO> GetAvailable(int pageNumberZeroIndexed, int pageSize)
        {
            var numberOfElementsToSkip = pageNumberZeroIndexed * pageSize;
            var entities = DbSet
                .Where(p => p.IsAvailable)
                .Skip(numberOfElementsToSkip)
                .Take(pageSize)
                .ToList();

            return MapList<Product, ProductDTO>(entities);
        }

        public IEnumerable<ProductDTO> GetFiltered(int? typeId, int? categoryId, int? unitId)
        {
            var entitiesQuery = DbSet.AsQueryable();

            if (typeId != null)
            {
                entitiesQuery = entitiesQuery.Where(p => p.TypeId == typeId);
            }

            if (categoryId != null)
            {
                entitiesQuery = entitiesQuery
                    .Where(p => p.CategoriesLink.Any(cl => cl.CategoryId == categoryId));
            }

            if (unitId != null)
            {
                entitiesQuery = entitiesQuery.Where(p => p.UnitId == unitId);
            }

            var entities = entitiesQuery.ToList();

            return MapList<Product, ProductDTO>(entities);
        }

        public ProductInfoDTO GetProductInfo(int productId)
        {
            var entity = DbSet
                .Include(p => p.CategoriesLink)
                .Include(p => p.Type)
                .Include(p => p.Unit)
                .SingleOrDefault(p => p.Id == productId);

            return Map<Product, ProductInfoDTO>(entity);
        }
    }
}
