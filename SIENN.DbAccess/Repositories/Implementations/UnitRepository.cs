﻿using AutoMapper;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.DTOs;
using SIENN.Models;

namespace SIENN.DbAccess.Repositories.Implementations
{
    public class UnitRepository : GenericRepository<UnitDTO, Unit>, IUnitRepository
    {
        public UnitRepository(SIENNContext context, IMapper mapper) : base(context, mapper) { }
    }
}
