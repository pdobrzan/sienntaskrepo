﻿using AutoMapper;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.DTOs;
using SIENN.Models;

namespace SIENN.DbAccess.Repositories.Implementations
{
    public class TypeRepository : GenericRepository<TypeDTO, Type>, ITypeRepository
    {
        public TypeRepository(SIENNContext context, IMapper mapper) : base(context, mapper) { }
    }
}
