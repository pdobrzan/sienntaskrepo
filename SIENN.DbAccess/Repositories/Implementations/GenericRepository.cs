﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.Models;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.DbAccess.Repositories.Implementations
{
    public abstract class GenericRepository<TEntityDTO, TEntity>
        : IGenericRepository<TEntityDTO, TEntity>
        where TEntityDTO : class
        where TEntity : class
    {
        protected GenericRepository(SIENNContext context, IMapper mapper)
        {
            _context = context;
            this.mapper = mapper;
            DbSet = context.Set<TEntity>();
        }

        public virtual TEntityDTO Get(int id)
        {
            var entity = DbSet.Find(id);
            return mapper.Map<TEntityDTO>(entity);
        }

        public virtual IEnumerable<TEntityDTO> GetAll()
        {
            var entities = DbSet.ToList();
            return MapList<TEntity, TEntityDTO>(entities);
        }

        public virtual int Count()
        {
            return DbSet.Count();
        }

        public virtual void Add(TEntityDTO entityDTO)
        {
            var entity = Map<TEntityDTO, TEntity>(entityDTO);
            DbSet.Add(entity);
        }

        public virtual void Update(TEntityDTO entityDTO)
        {
            var entity = Map<TEntityDTO, TEntity>(entityDTO);
            DbSet.Attach(entity);
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
        }

        public virtual void Remove(int id)
        {
            var entity = DbSet.Find(id);
            DbSet.Remove(entity);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        protected TTo Map<TFrom, TTo>(TFrom fromModel)
        {
            return mapper.Map<TFrom, TTo>(fromModel);
        }

        protected IEnumerable<TTo> MapList<TFrom, TTo>(IEnumerable<TFrom> fromModel)
        {
            return mapper.Map<IEnumerable<TFrom>, IEnumerable<TTo>>(fromModel);
        }

        public DbSet<TEntity> DbSet { get; set; }

        private DbContext _context;
        private readonly IMapper mapper;
    }
}
