﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace SIENN.DbAccess.Repositories.Interfaces
{
    public interface IGenericRepository<TEntityDTO, TEntity>
        where TEntityDTO : class
        where TEntity : class
    {
        DbSet<TEntity> DbSet { get; set; }

        TEntityDTO Get(int id);

        IEnumerable<TEntityDTO> GetAll();

        int Count();

        void Add(TEntityDTO entity);
        void Update(TEntityDTO entity);
        void Remove(int id);

        void Save();
    }
}
