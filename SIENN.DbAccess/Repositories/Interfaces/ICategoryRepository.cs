﻿using SIENN.DTOs;
using SIENN.Models;

namespace SIENN.DbAccess.Repositories.Interfaces
{
    public interface ICategoryRepository : IGenericRepository<CategoryDTO, Category> { }
}
