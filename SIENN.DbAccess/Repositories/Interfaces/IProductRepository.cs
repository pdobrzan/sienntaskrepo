﻿using SIENN.DTOs;
using SIENN.Models;
using System.Collections.Generic;

namespace SIENN.DbAccess.Repositories.Interfaces
{
    public interface IProductRepository : IGenericRepository<ProductDTO, Product>
    {
        IEnumerable<ProductDTO> GetAvailable(int pageNumberZeroIndexed, int pageSize);
        IEnumerable<ProductDTO> GetFiltered(int? typeId, int? categoryId, int? unitId);
        ProductInfoDTO GetProductInfo(int productId);
    }
}
