﻿using System.Collections.Generic;

namespace SIENN.DTOs
{
    public class TypeDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public List<ProductDTO> Products { get; set; }
    }
}
