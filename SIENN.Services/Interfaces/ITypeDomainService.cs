﻿using SIENN.DTOs;

namespace SIENN.Services.Interfaces
{
    public interface ITypeDomainService : IGenericDomainService<TypeDTO>
    {
    }
}
