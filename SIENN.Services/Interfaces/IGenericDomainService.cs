﻿namespace SIENN.Services.Interfaces
{
    public interface IGenericDomainService<TEntityDTO> where TEntityDTO : class
    {
        void Add(TEntityDTO entity);
        TEntityDTO Get(int id);
        void Update(TEntityDTO entity);
        void Remove(int id);
    }
}
