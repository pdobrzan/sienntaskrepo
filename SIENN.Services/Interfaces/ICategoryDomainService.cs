﻿using SIENN.DTOs;

namespace SIENN.Services.Interfaces
{
    public interface ICategoryDomainService : IGenericDomainService<CategoryDTO>
    {
        CategoryDTO GetFirst();
    }
}
