﻿using SIENN.DTOs;
using System.Collections.Generic;

namespace SIENN.Services.Interfaces
{
    public interface IProductDomainService : IGenericDomainService<ProductDTO>
    {
        IEnumerable<ProductDTO> GetAvailable(int pageNumberZeroIndexed, int pageSize);
        IEnumerable<ProductDTO> GetFiltered(int? typeId, int? categoryId, int? unitId);
        ProductInfoDTO GetProductInfo(int productId);
    }
}
