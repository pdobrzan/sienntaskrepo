﻿using SIENN.DTOs;

namespace SIENN.Services.Interfaces
{
    public interface IUnitDomainService : IGenericDomainService<UnitDTO>
    {
    }
}
