﻿using SIENN.DTOs;
using SIENN.DbAccess.Repositories.Interfaces;
using AutoMapper;
using SIENN.Models;

namespace SIENN.Services.Interfaces
{
    public class TypeDomainService : GenericDomainService<TypeDTO, Type>, ITypeDomainService
    {
        private readonly ITypeRepository typeRepository;
        private readonly IMapper mapper;

        public TypeDomainService(ITypeRepository typeRepository, IMapper mapper)
            : base(typeRepository, mapper)
        {
            this.typeRepository = typeRepository;
            this.mapper = mapper;
        }
    }
}
