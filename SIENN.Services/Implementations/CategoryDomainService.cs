﻿using SIENN.DTOs;
using SIENN.DbAccess.Repositories.Interfaces;
using System.Linq;
using AutoMapper;
using SIENN.Models;

namespace SIENN.Services.Interfaces
{
    public class CategoryDomainService : GenericDomainService<CategoryDTO, Category>, ICategoryDomainService
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IMapper mapper;

        public CategoryDomainService(ICategoryRepository categoryRepository, IMapper mapper)
            : base(categoryRepository, mapper)
        {
            this.categoryRepository = categoryRepository;
            this.mapper = mapper;
        }

        public CategoryDTO GetFirst()
        {
            return categoryRepository.GetAll().FirstOrDefault();
        }
    }
}
