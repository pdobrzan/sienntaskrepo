﻿using SIENN.DTOs;
using SIENN.DbAccess.Repositories.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using SIENN.Models;

namespace SIENN.Services.Interfaces
{
    public class ProductDomainService : GenericDomainService<ProductDTO, Product>, IProductDomainService
    {
        private readonly IProductRepository productRepository;
        private readonly IMapper mapper;

        public ProductDomainService(IProductRepository productRepository, IMapper mapper)
            : base(productRepository, mapper)
        {
            this.productRepository = productRepository;
            this.mapper = mapper;
        }

        public IEnumerable<ProductDTO> GetAvailable(int pageNumberZeroIndexed, int pageSize)
        {
            return productRepository.GetAvailable(pageNumberZeroIndexed, pageSize);
        }

        public IEnumerable<ProductDTO> GetFiltered(int? typeId, int? categoryId, int? unitId)
        {
            return productRepository.GetFiltered(typeId, categoryId, unitId);
        }

        public ProductInfoDTO GetProductInfo(int productId)
        {
            return productRepository.GetProductInfo(productId);
        }
    }
}
