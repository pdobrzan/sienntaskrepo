﻿using SIENN.DbAccess.Repositories.Interfaces;
using AutoMapper;

namespace SIENN.Services.Interfaces
{
    public class GenericDomainService<TEntityDTO, TEntity>
        : IGenericDomainService<TEntityDTO>
        where TEntityDTO : class
        where TEntity : class
    {
        private readonly IGenericRepository<TEntityDTO, TEntity> repository;
        private readonly IMapper mapper;

        public GenericDomainService(IGenericRepository<TEntityDTO, TEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public void Add(TEntityDTO entityDTO)
        {
            repository.Add(entityDTO);
            repository.Save();
        }

        public TEntityDTO Get(int id)
        {
            return repository.Get(id);
        }

        public void Remove(int id)
        {
            repository.Remove(id);
            repository.Save();
        }

        public void Update(TEntityDTO entityDTO)
        {
            repository.Update(entityDTO);
            repository.Save();
        }
    }
}
