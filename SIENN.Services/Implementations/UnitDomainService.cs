﻿using SIENN.DTOs;
using SIENN.DbAccess.Repositories.Interfaces;
using System.Linq;
using AutoMapper;
using SIENN.Models;

namespace SIENN.Services.Interfaces
{
    public class UnitDomainService : GenericDomainService<UnitDTO, Unit>, IUnitDomainService
    {
        private readonly IUnitRepository unitRepository;
        private readonly IMapper mapper;

        public UnitDomainService(IUnitRepository unitRepository, IMapper mapper)
            : base(unitRepository, mapper)
        {
            this.unitRepository = unitRepository;
            this.mapper = mapper;
        }
    }
}
