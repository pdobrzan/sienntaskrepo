﻿-- returns Products assigned to more than one Category

select p.Id as ProductId, count(*) as CategoryCount
from Products p
join ProductCategory pc on pc.ProductId = p.Id
group by p.Id
having count(*) > 1