﻿using AutoMapper;
using SIENN.DTOs;
using SIENN.Models;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.Mapper
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        : this("MyProfile")
        { }

        protected AutoMapperProfileConfiguration(string profileName)
        : base(profileName)
        {
            CreateMap<Category, CategoryDTO>()
                .ForMember(dest => dest.Products,
                           opts => opts.MapFrom(src => src.ProductsLink.Select(pl => pl.Product)));

            CreateMap<CategoryDTO, Category>()
                .ForMember(dest => dest.ProductsLink,
                           opts => opts.MapFrom(src =>
                               src.Products.Select(p => new ProductCategory
                               {
                                   CategoryId = src.Id,
                                   ProductId = p.Id
                               })));

            CreateMap<ProductCategory, ProductCategoryDTO>()
                .ReverseMap();

            CreateMap<Product, ProductDTO>()
                .ForMember(dest => dest.Categories,
                           opts => opts.MapFrom(src => src.CategoriesLink.Select(pl => pl.Category).ToList()));

            CreateMap<Product, ProductInfoDTO>()
                .ForMember(dest => dest.ProductDescription,
                           opts => opts.MapFrom(src => $"({src.Code}) {src.Description}"))
                .ForMember(dest => dest.Price,
                           opts => opts.MapFrom(src => $"{src.Price:C}"))
                .ForMember(dest => dest.IsAvailable,
                           opts => opts.MapFrom(src => src.IsAvailable ? "Dostępny" : "Niedostępny"))
                .ForMember(dest => dest.DeliveryDate,
                           opts => opts.MapFrom(src => src.DeliveryDate.ToString("dd.MM.yyyy")))
                .ForMember(dest => dest.CategoriesCount,
                           opts => opts.MapFrom(src => src.CategoriesLink.Count))
                .ForMember(dest => dest.Type,
                           opts => opts.MapFrom(src => $"({src.Type.Code}) {src.Type.Description}"))
                .ForMember(dest => dest.Unit,
                           opts => opts.MapFrom(src => $"({src.Unit.Code}) {src.Unit.Description}"));

            CreateMap<ProductDTO, Product>()
                .ForMember(dest => dest.CategoriesLink,
                           opts => opts.MapFrom(src =>
                               src.Categories.Select(c => new ProductCategory
                               {
                                   ProductId = src.Id,
                                   CategoryId = c.Id
                               })));

            CreateMap<Type, TypeDTO>()
                .ReverseMap();

            CreateMap<Unit, UnitDTO>()
                .ReverseMap();
        }
    }
}
