﻿-- returns unavailable Products that will be delivered in current month

select * from Products
where IsAvailable = 0
and Month(DeliveryDate) = Month(getDate())
and DeliveryDate > getDate()