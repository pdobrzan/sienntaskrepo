﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIENN.Models
{
    public class Type
    {
        public int Id { get; set; }

        [Required]
        [StringLength(5)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public List<Product> Products { get; set; }
    }
}
