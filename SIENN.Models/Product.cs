﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIENN.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(5)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }


        [Required]
        public decimal Price { get; set; }

        [Required]
        public bool IsAvailable { get; set; }

        [Required]
        public DateTime DeliveryDate { get; set; }

        public int TypeId { get; set; }
        public Type Type { get; set; }

        public int UnitId { get; set; }
        public Unit Unit { get; set; }

        public List<ProductCategory> CategoriesLink { get; set; }
    }
}
