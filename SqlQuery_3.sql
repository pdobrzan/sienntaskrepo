﻿-- returns three Categories having highest average Product price

select top 3
c.Id as CategoryId, COUNT(*) as ProductCount, AVG(p.Price) as AverageProductPrice
from ProductCategory pc
join Products p on p.Id = pc.ProductId
join Categories c on c.Id = pc.CategoryId
where p.IsAvailable = 1
group by c.Id
order by AVG(p.Price) desc