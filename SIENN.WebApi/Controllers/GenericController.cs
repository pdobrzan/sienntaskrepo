﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Services.Interfaces;

namespace SIENN.WebApi.Controllers
{
    public class GenericController<T> : Controller where T: class
    {
        private readonly IGenericDomainService<T> genericDomainService;

        public GenericController(IGenericDomainService<T> genericDomainService)
        {
            this.genericDomainService = genericDomainService;
        }

        [HttpPut]
        public void Add([FromBody] T entity) => genericDomainService.Add(entity);

        [HttpGet]
        public T Get(int id) => genericDomainService.Get(id);

        [HttpPost]
        public void Update([FromBody] T entity) => genericDomainService.Update(entity);

        [HttpDelete]
        public void Remove(int id) => genericDomainService.Remove(id);
    }
}
