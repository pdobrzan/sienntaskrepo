﻿using SIENN.DTOs;
using SIENN.Services.Interfaces;

namespace SIENN.WebApi.Controllers
{
    public class TypeController : GenericController<TypeDTO>
    {
        private readonly ITypeDomainService typeDomainService;

        public TypeController(ITypeDomainService typeDomainService)
            : base(typeDomainService)
        {
            this.typeDomainService = typeDomainService;
        }
    }
}
