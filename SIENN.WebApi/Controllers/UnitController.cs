﻿using SIENN.DTOs;
using SIENN.Services.Interfaces;

namespace SIENN.WebApi.Controllers
{
    public class UnitController : GenericController<UnitDTO>
    {
        private readonly IUnitDomainService unitDomainService;

        public UnitController(IUnitDomainService unitDomainService)
            : base(unitDomainService)
        {
            this.unitDomainService = unitDomainService;
        }
    }
}
