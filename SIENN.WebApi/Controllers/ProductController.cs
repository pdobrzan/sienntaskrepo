﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DTOs;
using SIENN.Services.Interfaces;
using System.Collections.Generic;

namespace SIENN.WebApi.Controllers
{
    public class ProductController : GenericController<ProductDTO>
    {
        private readonly IProductDomainService productDomainService;

        public ProductController(IProductDomainService productDomainService)
            : base(productDomainService)
        {
            this.productDomainService = productDomainService;
        }

        [HttpGet]
        public IEnumerable<ProductDTO> GetAvailable(int pageNumberZeroIndexed, int pageSize)
        {
            return productDomainService.GetAvailable(pageNumberZeroIndexed, pageSize);
        }

        [HttpGet]
        public IEnumerable<ProductDTO> GetFiltered(int? typeId, int? categoryId, int? unitId)
        {
            return productDomainService.GetFiltered(typeId, categoryId, unitId);
        }

        [HttpGet]
        public ProductInfoDTO GetProductInfo(int productId)
        {
            return productDomainService.GetProductInfo(productId);
        }
    }
}
