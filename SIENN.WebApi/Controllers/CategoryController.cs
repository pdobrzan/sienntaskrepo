﻿using SIENN.DTOs;
using SIENN.Services.Interfaces;

namespace SIENN.WebApi.Controllers
{
    public class CategoryController : GenericController<CategoryDTO>
    {
        private readonly ICategoryDomainService categoryDomainService;

        public CategoryController(ICategoryDomainService categoryDomainService)
            : base(categoryDomainService)
        {
            this.categoryDomainService = categoryDomainService;
        }
    }
}
