﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories.Interfaces;

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class SiennController : Controller
    {
        private readonly ICategoryRepository categoryRepository;

        public SiennController(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        [HttpGet]
        public string Get() => "SIENN Poland";

        [HttpGet]
    [Route("api/[controller]")]
        public string Get2() => categoryRepository.Get(1).Description;
    }
}
