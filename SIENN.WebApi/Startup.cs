﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SIENN.Models;
using SIENN.DbAccess.Repositories.Implementations;
using SIENN.DbAccess.Repositories.Interfaces;
using SIENN.Services.Interfaces;
using Swashbuckle.AspNetCore.Swagger;
using SIENN.Mapper;

namespace SIENN.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "SIENN Recruitment API"
                });
            });

            var connectionString = Configuration.GetConnectionString("SIENNDb");
            services.AddDbContext<SIENNContext>(options => options.UseSqlServer(connectionString));

            services.AddAutoMapper(typeof(AutoMapperProfileConfiguration));
            services.AddMvc();

            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ITypeRepository, TypeRepository>();
            services.AddScoped<IUnitRepository, UnitRepository>();

            services.AddScoped<ICategoryDomainService, CategoryDomainService>();
            services.AddScoped<IProductDomainService, ProductDomainService>();
            services.AddScoped<ITypeDomainService, TypeDomainService>();
            services.AddScoped<IUnitDomainService, UnitDomainService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIENN Recruitment API v1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}");
            });
        }
    }
}
